import unittest
from fractions import Fraction

from my_sum.func import sum

class TestSum(unittest.TestCase):
    def test_list_int(self):
        data = [1, 2, 3]
        result = sum(data)
        self.assertEqual(result, 6)


    def test_tuple_int(self):
        data = (1, 2, 3)
        result = sum(data)
        self.assertEqual(result, 6)

    def test_list_float(self):
        data = [1.5, 2.5, 3.5]
        result = sum(data)
        self.assertEqual(result, 7.5)

    def test_negative_int(self):
        data = [1, -2, 3]
        result = sum(data)
        self.assertEqual(result, 2)

    def test_set_input_input(self):
        data = set(1, 2, 3, "34", 10.3, "10.5")
        result = sum(data)
        self.assertEqual(result, 60.8)

    def test_str_input(self):
        data = ("1", "1.6", "1E2")
        result = sum(data)
        self.assertEqual(result, 102.6)



    def test_list_fraction(self):
        data = [Fraction(1, 4), Fraction(1, 4), Fraction(2, 5)]
        result = sum(data)
        self.assertEqual(result, Fraction(9, 10))



if __name__ == "__main__":
    unittest.main()



